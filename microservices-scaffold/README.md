# microservices-scaffold

![Python package](https://github.com/python-microservices/microservices-scaffold/workflows/Python%20package/badge.svg?branch=master)
[![Build Status](https://travis-ci.org/python-microservices/microservices-scaffold.svg?branch=master)](https://travis-ci.org/python-microservices/microservices-scaffold)
[![Coverage Status](https://coveralls.io/repos/github/python-microservices/microservices-scaffold/badge.svg?branch=master)](https://coveralls.io/github/python-microservices/microservices-scaffold?branch=master)
[![Requirements Status](https://requires.io/github/python-microservices/microservices-scaffold/requirements.svg?branch=master)](https://requires.io/github/python-microservices/microservices-scaffold/requirements/?branch=master)
[![Updates](https://pyup.io/repos/github/python-microservices/microservices-scaffold/shield.svg)](https://pyup.io/repos/github/python-microservices/microservices-scaffold/)
[![Python 3](https://pyup.io/repos/github/python-microservices/microservices-scaffold/python-3-shield.svg)](https://pyup.io/repos/github/python-microservices/microservices-scaffold/)

Python Microservice Scaffold is an example of how to structure a Flask Microservice Project.
This Scaffold is build over [PyMS](https://github.com/python-microservices/pyms) package. PyMS is a 
[Microservice chassis pattern](https://microservices.io/patterns/microservice-chassis.html)
like Spring Boot (Java) or Gizmo (Golang). PyMS is a collection of libraries, best practices and recommended ways to build
microservices with Python which handles cross-cutting concerns:
- Externalized configuration
- Logging
- Health checks
- Metrics
- Distributed tracing

## Quickstart

See our [quickstart webpage](https://python-microservices.github.io/scaffold/quickstart/)

# How To contribute

We appreciate opening issues and pull requests to make PyMS even more stable & useful! See [This doc](CONTRIBUTING.md)
for more details



Installation
Clone the repository

git clone git@github.com:purwowd/microservices-scaffold.git
cd microservices-scaffold
Install with virtualenv

virtualenv --python=python[3.6|3.7|3.8] venv
source venv/bin/activate
pip install -r requirements.txt
Install with pipenv

pip install pipenv
pipenv install
Install on MacOS

virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
python manage.py runserver
Advantages over plain pip and requirements.txt
Pipenv generates two files: a Pipfileand a Pipfile.lock. * Pipfile: Is a high level declaration of the dependencies of your project. It can contain "dev" dependencies (usually test related stuff) and "standard" dependencies which are the ones you'll need for your project to function * Pipfile.lock: Is the "list" of all the dependencies your Pipfile has installed, along with their version and their hashes. This prevents two things: Conflicts between dependencies and installing a malicious module.

For a more in-depth explanation please refer to the official documentation.

Run your python script

python manage.py runserver
Check the result
Your default endpoints will be in this url:


http://127.0.0.1:5000/films
http://127.0.0.1:5000/actors
This URL is set in your config.yml:


pyms:
config:
DEBUG: false
TESTING: false
APP_NAME: Template
APPLICATION_ROOT : "" # <!---
You can acceded to a swagger ui in the next url:


http://127.0.0.1:5000/ui/
This PATH is set in your config.yml:


pyms:
services:
swagger:
path: "swagger"
file: "swagger.yaml"
url: "/ui/" # <!---
Template
You can create your own project from template: github.com/python-microservices/microservices-template